"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Namen = require("namen");
class Keim {
    constructor(stringSeed = "keim") {
        // Sets a seed for all future calculations
        this.intSeed = this.calculateIntSeed(stringSeed);
    }
    /**
     * Calculates an intSeed from the stringSeed supplied by the user
     * @param input
     * @returns {number}
     */
    calculateIntSeed(input) {
        let randomSeed = 0;
        for (let i = 0; i < input.length; i++) {
            randomSeed += (input[i].charCodeAt() - 96);
        }
        return randomSeed;
    }
    /**
     * Creates a random number between 0 and 1. This is used in all below functions.
     * @returns {number}
     */
    random() {
        let x = Math.sin(this.intSeed++) * 10000;
        return x - Math.floor(x);
    }
    /**
     * Creates a random string in a given length.
     * @param length
     * @returns {string}
     */
    createRandomString(length = 32) {
        const chars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        let result = '';
        for (var i = length; i > 0; --i)
            result += chars[Math.floor(this.random() * chars.length)];
        return result;
    }
    /**
     * Creates a random number between min and max, distributed linearly.
     * @param min
     * @param max
     * @param intOnly
     * @returns {*}
     */
    createRandomLinearBetween(min, max, intOnly = false) {
        let random = this.random();
        if (!intOnly) {
            return random * (max - min) + min;
        }
        else {
            return Math.floor(random * (max - min + 1) + min);
        }
    }
    createNRandomLinearBetween(n, min, max, intOnly = false) {
        let results = [];
        for (let i = 0; i++; i < n) {
            results.push(this.createRandomLinearBetween(min, max, intOnly));
        }
        return results;
    }
    /**
     * Creates a random number in a range maxDelta around mean.
     * @param mean
     * @param maxDelta
     * @param intOnly
     * @returns {*}
     */
    createRandomLinearAround(mean, maxDelta, intOnly = false) {
        return this.createRandomLinearBetween(mean - maxDelta, mean + maxDelta, intOnly);
    }
    createNRandomLinearAround(n, mean, maxDelta, intOnly = false) {
        let results = [];
        for (let i = 0; i++; i < n) {
            results.push(this.createRandomLinearAround(mean, maxDelta, intOnly));
        }
        return results;
    }
    /**
     * Creates a random number with normal distribution (mean = 0, sd = 1)
     * @returns {number}
     */
    createRandomNormal() {
        const u = 1 - this.random();
        const v = 1 - this.random();
        return Math.sqrt(-2.0 * Math.log(u)) * Math.cos(2.0 * Math.PI * v);
    }
    createNRandomNormal(n) {
        let results = [];
        for (let i = 0; i++; i < n) {
            results.push(this.createRandomNormal());
        }
        return results;
    }
    /**
     * Creates a random number with a gaussian distribution (mean and sd as given)
     * @param mean
     * @param sigma
     * @param intOnly
     * @returns {*}
     */
    createRandomGaussian(mean, sigma, lowerCut, upperCut, intOnly = false) {
        let candidate;
        if (!intOnly) {
            candidate = (this.createRandomNormal() * sigma + mean);
        }
        else {
            candidate = Math.floor(this.createRandomNormal() * sigma + mean + 0.5);
        }
        if (lowerCut && lowerCut > candidate)
            return lowerCut;
        if (upperCut && upperCut < candidate)
            return upperCut;
        return candidate;
    }
    createNRRandomGaussian(n, mean, sigma, lowerCut, upperCut, intOnly = false) {
        let results = [];
        for (let i = 0; i++; i < n) {
            results.push(this.createRandomGaussian(mean, sigma, lowerCut, upperCut, intOnly));
        }
        return results;
    }
    /**
     * Picks a random element from an array.
     * @param {array} array An array to pick an element from.
     * @return {*} A ramdom element from the array.
     */
    pickRandomElementFromArray(array) {
        const arrayLength = array.length;
        return array[this.createRandomLinearBetween(0, arrayLength - 1, true)];
    }
    /**
     * Returns a random first name.
     * @return {string} A first name
     */
    createRandomFirstName() {
        return this.pickRandomElementFromArray(Namen.firstNames);
    }
    /**
     * Returns a random last name.
     * @return {string} A last name
     */
    createRandomLastName() {
        return this.pickRandomElementFromArray(Namen.lastNames);
    }
    /**
     * Creates a Monte Carlo Simulation environment.
     * @return {*} A Monte Carlo Simulation environment
     */
    createMonteCarloEnvironment() {
        const keim = this;
        const mce = {
            options: [],
            optionsNormalized: [],
            addOption: (data, size) => {
                mce.options.push({
                    data,
                    size
                });
            },
            normalizeOptions: () => {
                // Sum up all sizes
                let sumOfSizes = 0;
                mce.options.forEach(option => {
                    sumOfSizes += option.size;
                });
                // Find scaling factor
                const scalingFactor = 1 / sumOfSizes;
                // Scale sizes and calculate lower and upper bounds
                let bound = 0;
                mce.options.forEach(option => {
                    const size = option.size * scalingFactor;
                    mce.optionsNormalized.push({
                        "data": option.data,
                        "lower": bound,
                        "upper": bound + size,
                        "size": size
                    });
                    bound += size;
                });
            },
            rollDice: () => {
                const random = keim.createRandomLinearBetween(0, 1, false);
                const result = mce.optionsNormalized.find((option) => (option.lower <= random && option.upper >= random));
                return result;
            }
        };
        return mce;
    }
}
exports.default = Keim;
